uint32_t get_largest_component(uint32_t r, uint32_t g, uint32_t b){
  if(r > g){
    if(r > b){
      return r;
    }
    else{
      return b;
    }
  }
  else if(g > b){
    return g;
  }
  else{
    return b;
  }
}

void changeIntensity(char up_or_down, char ceil_or_wall, int unit_index){
  uint32_t color;
  if(ceil_or_wall == 0){
    color = units[unit_index].ceil_color;
  }
  else{
    color = units[unit_index].wall_color;
  }

  uint32_t r = (color >> 16) % 256;
  uint32_t g = (color >> 8) % 256;
  uint32_t b = color % 256;

  if(up_or_down == 0){
      r = 255 - r;
      g = 255 - g;
      b = 255 - b;
  }

  uint32_t largest_component = get_largest_component(r, g, b);
  if(!largest_component){
    r = 16;
    g = 16;
    b = 16;
  }
  else if(largest_component <= 239){
    r = r + (uint32_t)(((double)r /(double)largest_component) * (double) 16);
    g = g + (uint32_t)(((double)g /(double)largest_component) * (double) 16);
    b = b + (uint32_t)(((double)b /(double)largest_component) * (double) 16);
  }
  else if(largest_component != 255){
    double inc = 255 - largest_component;
    r = r + (uint32_t)(((double)r /(double)largest_component) * inc);
    g = g + (uint32_t)(((double)g /(double)largest_component) * inc);
    b = b + (uint32_t)(((double)b /(double)largest_component) * inc);
  }

  if(up_or_down == 0){
    r = 255 - r;
    g = 255 - g;
    b = 255 - b;
  }

  if(ceil_or_wall == 0){
    units[unit_index].ceil_color;
  }
  else{
    units[unit_index].wall_color;
  }
}
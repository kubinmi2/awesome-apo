/*******************************************************************
  Simple program to check LCD functionality on MicroZed
  based MZ_APO board designed by Petr Porazil at PiKRON

  mzapo_lcdtest.c       - main and only file

  (C) Copyright 2004 - 2017 by Pavel Pisa
      e-mail:   pisa@cmp.felk.cvut.cz
      homepage: http://cmp.felk.cvut.cz/~pisa
      work:     http://www.pikron.com/
      license:  any combination GPL, LGPL, MPL or BSD licenses

 *******************************************************************/



#define _POSIX_C_SOURCE 200112L

#include <sys/mman.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <fcntl.h>
#include <malloc.h>
#include <string.h>
#include <byteswap.h>
#include <getopt.h>
#include <inttypes.h>
#include <time.h>
#include <string.h>
#include "fonts.h"
#include <pthread.h>
#include <errno.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <ifaddrs.h>


char *memdev="/dev/mem";

#define SPILED_REG_BASE_PHYS 0x43c40000
#define SPILED_REG_SIZE      0x00004000

#define SPILED_REG_LED_LINE_o           0x004
#define SPILED_REG_LED_RGB1_o           0x010
#define SPILED_REG_LED_RGB2_o           0x014
#define SPILED_REG_LED_KBDWR_DIRECT_o   0x018

#define SPILED_REG_KBDRD_KNOBS_DIRECT_o 0x020
#define SPILED_REG_KNOBS_8BIT_o         0x024

#define PARLCD_REG_BASE_PHYS 0x43c00000
#define PARLCD_REG_SIZE      0x00004000

#define PARLCD_REG_CMD_o                0x0008
#define PARLCD_REG_DATA_o               0x000C

#define UNITS_MAX 10

#define BLACK 0x00000000
#define WHITE 0x00ffffff
#define LIGHT_GRAY 0x00e0e0e0
#define GRAY 0x00808080
#define DARK_GRAY 0x00404040
#define RED 0x00ff0000
#define PINK 0x00ff60d0
#define PURPLE 0x00a020ff
#define LIGHT_BLUE 0x0050d0ff
#define BLUE 0x000020ff
#define LIGHT_GREEN 0x0060ff80
#define GREEN 0x0000ff00
#define YELLOW 0x00ffe020
#define ORANGE 0x00ffa010
#define BROWN 0x00a08060
#define BEIGE 0x00ffd0a0

#define MY_PORT 55555 
#define SWAP(x) ((x>>24)&0xff)|((x<<8)&0xff0000)|((x>>8)&0xff00)|((x<<24)&0xff000000)
#define SWAP_S(x) ((x&0xffff)>>8)|((x&0xffff)<<8)
#define HOST_SIZE 1024

char BIG_END;
typedef struct{
  char exists;
  char inSubmenu;

  char name[17];
  uint32_t ceil_color;
  uint32_t wall_color;
  uint16_t icon_buffer[256];
  uint32_t IP;

} units_struct;

//global variables
units_struct units[UNITS_MAX];
int UNITS_INDEX[UNITS_MAX];
unsigned char* parlcd_mem_base;
unsigned char *spiled_mem_base;
int MY_UNIT_INDEX;
int sockfd;
int toBeSent = 0;
int indexOfSent;
struct sockaddr_in bindaddr;
struct sockaddr_in binaddr_other;

void *map_phys_address(off_t region_base, size_t region_size, int opt_cached)
{
  unsigned long mem_window_size;
  unsigned long pagesize;
  unsigned char *mm;
  unsigned char *mem;
  int fd;

  fd = open(memdev, O_RDWR | (!opt_cached? O_SYNC: 0));
  if (fd < 0) {
    fprintf(stderr, "cannot open %s\n", memdev);
    return NULL;
  }

  pagesize=sysconf(_SC_PAGESIZE);

  mem_window_size = ((region_base & (pagesize-1)) + region_size + pagesize-1) & ~(pagesize-1);

  mm = mmap(NULL, mem_window_size, PROT_WRITE|PROT_READ,
              MAP_SHARED, fd, region_base & ~(pagesize-1));
  mem = mm + (region_base & (pagesize-1));

  if (mm == MAP_FAILED) {
    fprintf(stderr,"mmap error\n");
    return NULL;
  }

  return mem;
}

void parlcd_write_cmd(unsigned char *parlcd_mem_base, uint16_t cmd)
{
  *(volatile uint16_t*)(parlcd_mem_base + PARLCD_REG_CMD_o) = cmd;
}

void parlcd_write_data(unsigned char *parlcd_mem_base, uint16_t data)
{
  *(volatile uint16_t*)(parlcd_mem_base + PARLCD_REG_DATA_o) = data;
}

void parlcd_write_data2x(unsigned char *parlcd_mem_base, uint32_t data)
{
  *(volatile uint32_t*)(parlcd_mem_base + PARLCD_REG_DATA_o) = data;
}

void parlcd_delay(int msec)
{
  struct timespec wait_delay = {.tv_sec = msec / 1000,
                                .tv_nsec = (msec % 1000) * 1000 * 1000};
  clock_nanosleep(CLOCK_MONOTONIC, 0, &wait_delay, NULL);
}

void parlcd_hx8357_init(unsigned char *parlcd_mem_base)
{
  // toggle RST low to reset
/*
    digitalWrite(_rst, HIGH);
    parlcd_delay(50);
    digitalWrite(_rst, LOW);
    parlcd_delay(10);
    digitalWrite(_rst, HIGH);
    parlcd_delay(10);
*/
    parlcd_write_cmd(parlcd_mem_base, 0x1);
    parlcd_delay(30);

#ifdef HX8357_B
// Configure HX8357-B display
    parlcd_write_cmd(parlcd_mem_base, 0x11);
    parlcd_delay(20);
    parlcd_write_cmd(parlcd_mem_base, 0xD0);
    parlcd_write_data(parlcd_mem_base, 0x07);
    parlcd_write_data(parlcd_mem_base, 0x42);
    parlcd_write_data(parlcd_mem_base, 0x18);

    parlcd_write_cmd(parlcd_mem_base, 0xD1);
    parlcd_write_data(parlcd_mem_base, 0x00);
    parlcd_write_data(parlcd_mem_base, 0x07);
    parlcd_write_data(parlcd_mem_base, 0x10);

    parlcd_write_cmd(parlcd_mem_base, 0xD2);
    parlcd_write_data(parlcd_mem_base, 0x01);
    parlcd_write_data(parlcd_mem_base, 0x02);

    parlcd_write_cmd(parlcd_mem_base, 0xC0);
    parlcd_write_data(parlcd_mem_base, 0x10);
    parlcd_write_data(parlcd_mem_base, 0x3B);
    parlcd_write_data(parlcd_mem_base, 0x00);
    parlcd_write_data(parlcd_mem_base, 0x02);
    parlcd_write_data(parlcd_mem_base, 0x11);

    parlcd_write_cmd(parlcd_mem_base, 0xC5);
    parlcd_write_data(parlcd_mem_base, 0x08);

    parlcd_write_cmd(parlcd_mem_base, 0xC8);
    parlcd_write_data(parlcd_mem_base, 0x00);
    parlcd_write_data(parlcd_mem_base, 0x32);
    parlcd_write_data(parlcd_mem_base, 0x36);
    parlcd_write_data(parlcd_mem_base, 0x45);
    parlcd_write_data(parlcd_mem_base, 0x06);
    parlcd_write_data(parlcd_mem_base, 0x16);
    parlcd_write_data(parlcd_mem_base, 0x37);
    parlcd_write_data(parlcd_mem_base, 0x75);
    parlcd_write_data(parlcd_mem_base, 0x77);
    parlcd_write_data(parlcd_mem_base, 0x54);
    parlcd_write_data(parlcd_mem_base, 0x0C);
    parlcd_write_data(parlcd_mem_base, 0x00);

    parlcd_write_cmd(parlcd_mem_base, 0x36);
    parlcd_write_data(parlcd_mem_base, 0x0a);

    parlcd_write_cmd(parlcd_mem_base, 0x3A);
    parlcd_write_data(parlcd_mem_base, 0x55);

    parlcd_write_cmd(parlcd_mem_base, 0x2A);
    parlcd_write_data(parlcd_mem_base, 0x00);
    parlcd_write_data(parlcd_mem_base, 0x00);
    parlcd_write_data(parlcd_mem_base, 0x01);
    parlcd_write_data(parlcd_mem_base, 0x3F);

    parlcd_write_cmd(parlcd_mem_base, 0x2B);
    parlcd_write_data(parlcd_mem_base, 0x00);
    parlcd_write_data(parlcd_mem_base, 0x00);
    parlcd_write_data(parlcd_mem_base, 0x01);
    parlcd_write_data(parlcd_mem_base, 0xDF);

    parlcd_delay(120);
    parlcd_write_cmd(parlcd_mem_base, 0x29);

    parlcd_delay(25);

#else
// HX8357-C display initialisation

    parlcd_write_cmd(parlcd_mem_base, 0xB9); // Enable extension command
    parlcd_write_data(parlcd_mem_base, 0xFF);
    parlcd_write_data(parlcd_mem_base, 0x83);
    parlcd_write_data(parlcd_mem_base, 0x57);
    parlcd_delay(50);

    parlcd_write_cmd(parlcd_mem_base, 0xB6); //Set VCOM voltage
    //parlcd_write_data(parlcd_mem_base, 0x2C);    //0x52 for HSD 3.0"
    parlcd_write_data(parlcd_mem_base, 0x52);    //0x52 for HSD 3.0"

    parlcd_write_cmd(parlcd_mem_base, 0x11); // Sleep off
    parlcd_delay(200);

    parlcd_write_cmd(parlcd_mem_base, 0x35); // Tearing effect on
    parlcd_write_data(parlcd_mem_base, 0x00);    // Added parameter

    parlcd_write_cmd(parlcd_mem_base, 0x3A); // Interface pixel format
    parlcd_write_data(parlcd_mem_base, 0x55);    // 16 bits per pixel

    //parlcd_write_cmd(parlcd_mem_base, 0xCC); // Set panel characteristic
    //parlcd_write_data(parlcd_mem_base, 0x09);    // S960>S1, G1>G480, R-G-B, normally black

    //parlcd_write_cmd(parlcd_mem_base, 0xB3); // RGB interface
    //parlcd_write_data(parlcd_mem_base, 0x43);
    //parlcd_write_data(parlcd_mem_base, 0x00);
    //parlcd_write_data(parlcd_mem_base, 0x06);
    //parlcd_write_data(parlcd_mem_base, 0x06);

    parlcd_write_cmd(parlcd_mem_base, 0xB1); // Power control
    parlcd_write_data(parlcd_mem_base, 0x00);
    parlcd_write_data(parlcd_mem_base, 0x15);
    parlcd_write_data(parlcd_mem_base, 0x0D);
    parlcd_write_data(parlcd_mem_base, 0x0D);
    parlcd_write_data(parlcd_mem_base, 0x83);
    parlcd_write_data(parlcd_mem_base, 0x48);


    parlcd_write_cmd(parlcd_mem_base, 0xC0); // Does this do anything?
    parlcd_write_data(parlcd_mem_base, 0x24);
    parlcd_write_data(parlcd_mem_base, 0x24);
    parlcd_write_data(parlcd_mem_base, 0x01);
    parlcd_write_data(parlcd_mem_base, 0x3C);
    parlcd_write_data(parlcd_mem_base, 0xC8);
    parlcd_write_data(parlcd_mem_base, 0x08);

    parlcd_write_cmd(parlcd_mem_base, 0xB4); // Display cycle
    parlcd_write_data(parlcd_mem_base, 0x02);
    parlcd_write_data(parlcd_mem_base, 0x40);
    parlcd_write_data(parlcd_mem_base, 0x00);
    parlcd_write_data(parlcd_mem_base, 0x2A);
    parlcd_write_data(parlcd_mem_base, 0x2A);
    parlcd_write_data(parlcd_mem_base, 0x0D);
    parlcd_write_data(parlcd_mem_base, 0x4F);

    parlcd_write_cmd(parlcd_mem_base, 0xE0); // Gamma curve
    parlcd_write_data(parlcd_mem_base, 0x00);
    parlcd_write_data(parlcd_mem_base, 0x15);
    parlcd_write_data(parlcd_mem_base, 0x1D);
    parlcd_write_data(parlcd_mem_base, 0x2A);
    parlcd_write_data(parlcd_mem_base, 0x31);
    parlcd_write_data(parlcd_mem_base, 0x42);
    parlcd_write_data(parlcd_mem_base, 0x4C);
    parlcd_write_data(parlcd_mem_base, 0x53);
    parlcd_write_data(parlcd_mem_base, 0x45);
    parlcd_write_data(parlcd_mem_base, 0x40);
    parlcd_write_data(parlcd_mem_base, 0x3B);
    parlcd_write_data(parlcd_mem_base, 0x32);
    parlcd_write_data(parlcd_mem_base, 0x2E);
    parlcd_write_data(parlcd_mem_base, 0x28);

    parlcd_write_data(parlcd_mem_base, 0x24);
    parlcd_write_data(parlcd_mem_base, 0x03);
    parlcd_write_data(parlcd_mem_base, 0x00);
    parlcd_write_data(parlcd_mem_base, 0x15);
    parlcd_write_data(parlcd_mem_base, 0x1D);
    parlcd_write_data(parlcd_mem_base, 0x2A);
    parlcd_write_data(parlcd_mem_base, 0x31);
    parlcd_write_data(parlcd_mem_base, 0x42);
    parlcd_write_data(parlcd_mem_base, 0x4C);
    parlcd_write_data(parlcd_mem_base, 0x53);
    parlcd_write_data(parlcd_mem_base, 0x45);
    parlcd_write_data(parlcd_mem_base, 0x40);
    parlcd_write_data(parlcd_mem_base, 0x3B);
    parlcd_write_data(parlcd_mem_base, 0x32);

    parlcd_write_data(parlcd_mem_base, 0x2E);
    parlcd_write_data(parlcd_mem_base, 0x28);
    parlcd_write_data(parlcd_mem_base, 0x24);
    parlcd_write_data(parlcd_mem_base, 0x03);
    parlcd_write_data(parlcd_mem_base, 0x00);
    parlcd_write_data(parlcd_mem_base, 0x01);

    parlcd_write_cmd(parlcd_mem_base, 0x36); // MADCTL Memory access control
    //parlcd_write_data(parlcd_mem_base, 0x48);
    parlcd_write_data(parlcd_mem_base, 0xE8);
    parlcd_delay(20);

    parlcd_write_cmd(parlcd_mem_base, 0x21); //Display inversion on
    parlcd_delay(20);

    parlcd_write_cmd(parlcd_mem_base, 0x29); // Display on

    parlcd_delay(120);
#endif
}

void delay(int number_of_seconds)
{
    // Converting time into milli_seconds
    int milli_seconds = 1000 * number_of_seconds;
 
    // Stroing start time
    clock_t start_time = clock();
 
    // looping till required time is not acheived
    while (clock() < start_time + milli_seconds)
        ;
}

void rand_string_num(char* string){
  srand(time(NULL));
  int i;
  for(i = 0; i < 8; i++){
    string[i] = (rand() % 10) +'0'; 
  }
  string[8] = '\0';
}

void generate_unique_name(char *name){
  name = strcat(name,"unit no.");
  rand_string_num(name + 8);
}

void generate_unique_icon(uint16_t *icon_buffer){
  int i, j;
  srand(time(NULL));
  uint16_t random_background = rand()%65536;
  for(i = 0; i < 256; i++){
    icon_buffer[i] = random_background;
  }
  uint16_t random_circle_color = rand()%65536;
  int radius = 8;
  int x0 = 8;
  int y0 = 8;

  for (i = y0-radius; i < y0+radius ; i++) {
    for (j = x0-radius; j < x0+radius; j++) {
      if(((j-x0)*(j-x0) + (i-y0)*(i-y0)) < radius*radius){	
        icon_buffer[i*16 + j] = random_circle_color;
      }
    }
  }
}

void init_unit_struct(){
  int i;

  generate_unique_name(units[0].name);
  units[0].ceil_color = 0x00006226;
  units[0].wall_color = 0x00ffffff;
  units[0].exists = 1;
  units[0].inSubmenu = 0;
  generate_unique_icon(units[0].icon_buffer);

  for(i = 1; i < UNITS_MAX; i++){
    units[i].exists = 0;
    units[i].inSubmenu = 0;
  }
}

char dicipher(int hash, int x){
  if((hash >> (15 - x))%2){
    return 1;
  } 
  else{
    return 0;
  }
}

void printBuffer(unsigned *buffer){
int i,j;
  for (i = 0; i < 320 ; i++) {
    for (j = 0; j < 480 ; j++) {
      parlcd_write_data(parlcd_mem_base, buffer[i*480 + j]);
    }
 }

}

void printChar(int lineOffset, int offsetX, int offsetY, unsigned char character, unsigned *buffer){
  int charWidth = winFreeSystem14x16_width[character - 32];
  int horizOffset = lineOffset + offsetX;
  int fontHashIndex = 16 * (character - 32);

  for(int y = 0; y < 16; y++){
    for(int x = 0; x < charWidth; x++){
      if(dicipher(winFreeSystem14x16_bits[fontHashIndex], x)){
        buffer[(y + offsetY) * 480 + (x + horizOffset)] = 0x000000;
      }
    }
    fontHashIndex++;
  }
}

void printText(int offsetX, int offsetY, unsigned char *string, unsigned *buffer){
  
  int stringIndex = 0;
  int lineOffset = 0;

  while(stringIndex < strlen((char *)string)){
    printChar(lineOffset, offsetX, offsetY, string[stringIndex], buffer);
    lineOffset += winFreeSystem14x16_width[string[stringIndex] - 32];
    stringIndex++;
  }  
}

void printIcon(int offsetX, int offsetY, uint16_t*icon_buffer, unsigned*buffer){
  int y,x;
  for(y = 0; y < 16; y++){
    for(x = 0; x < 16; x++){
      buffer[(y+offsetY)*480 + (x + offsetX)] = icon_buffer[(y*16) + x];
    }
  }
}

void draw_submenu_background(unsigned *buffer){
  int i, j;
  
  for (i = 0; i < 320; i++) {
    for (j = 0; j < 480 ; j++) {
      buffer[i*480 + j] = 0;
    }
  }

  for(int a = 0; a < 6; a++){
    for (i = 10; i < 50; i++) {
      for (j = 15; j < 465 ; j++) {
        buffer[(i + (a*52))*480 + j] = 0xFFFF;
      }
    }
  }
}

unsigned color_convert(uint32_t orig){
  unsigned color;
  unsigned red = (unsigned)((((double)((orig>>16)%256))/(double)(256))*(double)(32));
  unsigned green = (unsigned)((((double)((orig>>8)%256))/(double)(256))*(double)(64));
  unsigned blue = (unsigned)((((double)((orig)%256))/(double)(256))*(double)(32));
  color = (red << 11) + (green << 5) + blue;
  return color;
}

void printRectangle(unsigned*buffer, int x, int y, int choice, char ceil_or_wall){
  unsigned color;
  if(ceil_or_wall==0){
    color = color_convert(units[UNITS_INDEX[choice]].ceil_color);
  }
  else{
    color = color_convert(units[UNITS_INDEX[choice]].wall_color);
  }
  int i,j;

  for(j = -1; j < 51; j++){
      buffer[((y -1) * 480) + x + j] = BLACK;
      buffer[((y + 20) * 480) + x + j] = BLACK;
  }



  for(i = 0; i < 20; i++){
    buffer[(y + i) * 480 + x - 1] = BLACK;
    for(j = 0; j < 50; j++){
      buffer[(y + i) * 480 + x + j] = color;
    }
    buffer[(y + i) * 480 + x + 50] = BLACK;
  }
}
void printButtons(unsigned* buffer, int x0, int y0){
int radius = 9;
int i,j;
  i = 0;
  j = 0;
  for (i = y0-radius; i < y0+radius ; i++) {
    for (j = x0-radius; j < x0+radius; j++) {
      if(((j-x0)*(j-x0) + (i-y0)*(i-y0)) < radius*radius){	
        buffer[i*480 + j] = 0xf800;
      }
    }
  }
  x0 = x0 + 50;
  for (i = y0-radius; i < y0+radius ; i++) {
    for (j = x0-radius; j < x0+radius; j++) {
      if(((j-x0)*(j-x0) + (i-y0)*(i-y0)) < radius*radius){	
        buffer[i*480 + j] = 0x07e0;
      }
    }
  }
  x0 = x0 + 50;
  for (i = y0-radius; i < y0+radius ; i++) {
    for (j = x0-radius; j < x0+radius; j++) {
      if(((j-x0)*(j-x0) + (i-y0)*(i-y0)) < radius*radius){	
        buffer[i*480 + j] = 0x001f;
      }
    }
  }
}

void print_submenu_text(int choice, unsigned* buffer){
  printText(70, 22, (unsigned char*)units[UNITS_INDEX[choice]].name, buffer);
  printIcon(280, 22, units[UNITS_INDEX[choice]].icon_buffer, buffer);
  printText(70, 74, (unsigned char*)"Ceil color   :   change color   R          G          B", buffer);
  printButtons(buffer, 284, 82);
  printText(70, 126, (unsigned char*)"Change intensity", buffer);
  printRectangle(buffer, 284, 124, choice, 0);
  printText(70, 178, (unsigned char*)"Wall color   :   change color   R          G          B", buffer);
  printText(70, 230, (unsigned char*)"Change intensity", buffer);
  printRectangle(buffer, 284, 228, choice, 1);
  printButtons(buffer, 284, 186);

  printText(220, 282, (unsigned char*)"CONFIRM", buffer);
}

void submenu_button(unsigned *buffer, int choice){
int radius = 9;
int x0 = 30;
int y0 = 30 + choice * 52;
int i,j;
for (int k = 1; k < 6; ++k){
  int y0_temp = 30 + k * 52;

  for (i = y0_temp-radius; i < y0_temp+radius ; i++) {
    for (j = x0-radius; j < x0+radius; j++) {
      if(((j-x0)*(j-x0) + (i-y0_temp)*(i-y0_temp)) < radius*radius){	
        buffer[i*480 + j] = 0xFFFFFF;
      }
    }
  }
}
  i = 0;
  j = 0;
  for (i = y0-radius; i < y0+radius ; i++) {
    for (j = x0-radius; j < x0+radius; j++) {
      if(((j-x0)*(j-x0) + (i-y0)*(i-y0)) < radius*radius){	
        buffer[i*480 + j] = 0xf800;
      }
    }
  }
}
int isPressed(){
  uint32_t rgb_knobs_value;
     /* Initialize structure to 0 seconds and 200 milliseconds */
     //struct timespec loop_delay = {.tv_sec = 0, .tv_nsec = 200 * 1000 * 1000};
     rgb_knobs_value = *(volatile uint32_t*)(spiled_mem_base + SPILED_REG_KNOBS_8BIT_o);

    int press_button_new_int_val = (rgb_knobs_value>>26) % 2;
    return press_button_new_int_val;
}

int getIncrementation(unsigned int uint_val, unsigned int last_uint_val){
  
  if (uint_val > last_uint_val){
    if(uint_val == 63 && last_uint_val == 0){
          return 0;
    }
    else{
      return 1;
    }
    }else if (uint_val < last_uint_val){
      if(uint_val == 0 && last_uint_val == 63){
        return 1;
      }
      else{
        return 0;
      }
    }
    return 2;
}

void changeColorOfLedCeil(){
      *(volatile uint32_t*)(spiled_mem_base + SPILED_REG_LED_LINE_o) = units[UNITS_INDEX[0]].ceil_color;

      *(volatile uint32_t*)(spiled_mem_base + SPILED_REG_LED_RGB1_o) = units[UNITS_INDEX[0]].ceil_color;

}

void changeColorOfLedWall(){
      *(volatile uint32_t*)(spiled_mem_base + SPILED_REG_LED_LINE_o) = units[UNITS_INDEX[0]].wall_color;

      *(volatile uint32_t*)(spiled_mem_base + SPILED_REG_LED_RGB2_o) = units[UNITS_INDEX[0]].wall_color;
}


unsigned int getChangeOfColor(unsigned int temp, unsigned int uint_val, unsigned int last_uint_val){
  
  if (uint_val > last_uint_val){
    if(uint_val == 63 && last_uint_val == 0){
      if (((int)(temp) - 1) >= 0){
          temp--;
        }
    }
    else{
      if ((temp + 1) < 64){
        temp++;
      }
    }
    }else if (uint_val < last_uint_val){
      if(uint_val == 0 && last_uint_val == 63){
        if ((temp + 1) < 64){
        temp ++;
      }
      }
      else{
        if (((int)(temp) - 1) >= 0){
          temp--;
        }
      }
    }
    return temp;
}
          //changeColor(buffer, user_select, 0);
uint32_t get_largest_component(uint32_t color){
  uint32_t r = (color >> 16) % 256;
  uint32_t g = (color >> 8) % 256;
  uint32_t b = color % 256;

  if(r > g){
    if(r > b){
      return r;
    }
    else{
      return b;
    }
  }
  else if(g > b){
    return g;
  }
  else{
    return b;
  }
}

uint32_t getColorLimit(uint32_t color){
  uint32_t largest_component = get_largest_component(color);
  uint32_t r = (uint32_t)(((double)((color >> 16) % 256)/(double)largest_component) * 255.0);
  uint32_t g = (uint32_t)(((double)((color >> 8) % 256)/(double)largest_component) * 255.0);
  uint32_t b = (uint32_t)(((double)(color % 256)/(double)largest_component) * 255.0);
  return (r<<16) + (g<<8) + b;
}

void changeIntensityTest(int up_or_down, int ceil_or_wall, int choice, uint32_t color_limit){
  uint32_t color;
  if(ceil_or_wall == 0){
    color = units[UNITS_INDEX[choice]].ceil_color;
  }
  else{
    color = units[UNITS_INDEX[choice]].wall_color;
  }

  uint32_t r = (color >> 16) % 256;
  uint32_t g = (color >> 8) % 256;
  uint32_t b = color % 256;

  uint32_t redLimit = (color_limit >> 16) % 256;
  uint32_t greenLimit = (color_limit >> 8) % 256;
  uint32_t blueLimit = color_limit % 256;

  uint32_t largest_component = get_largest_component(color);

  if(up_or_down == 1){
    if(largest_component < 14){
      r = (double)((((double) redLimit / 255)) * 16.0);
      g = (double)((((double) greenLimit / 255)) * 16.0);
      b = (double)((((double) blueLimit / 255)) * 16.0);
    }
    else if(largest_component <= 239){
      r = r + (uint32_t)(((double)r /(double)largest_component) * (double) 16);
      g = g + (uint32_t)(((double)g /(double)largest_component) * (double) 16);
      b = b + (uint32_t)(((double)b /(double)largest_component) * (double) 16);
    }
    else if(largest_component != 255){
      double inc = 255 - largest_component;
      r = r + (uint32_t)(((double)r /(double)largest_component) * inc);
      g = g + (uint32_t)(((double)g /(double)largest_component) * inc);
      b = b + (uint32_t)(((double)b /(double)largest_component) * inc);
    }
  }
  else{
    if(largest_component >= 16){
      r = r - (uint32_t)(((double)r /(double)largest_component) * (double) 16);
      g = g - (uint32_t)(((double)g /(double)largest_component) * (double) 16);
      b = b - (uint32_t)(((double)b /(double)largest_component) * (double) 16);
    }
    else{
      r = 0;
      g = 0;
      b = 0;
    }
  }
  if(ceil_or_wall == 0){
    units[UNITS_INDEX[choice]].ceil_color = (r<<16) + (g<<8) + b;
        if(choice == 0){
          changeColorOfLedCeil();
        }
       

  }
  else{
    units[UNITS_INDEX[choice]].wall_color = (r<<16) + (g<<8) + b;
        if(choice == 0){
          changeColorOfLedWall();
        }
  }
}

void changeIntensity(unsigned *buffer, int user_select, int part_of_room){
  const int ceiling = 0;
  const int wall = 1;

      int isFirstTime = 1;
  /*
   * Setup memory mapping which provides access to the peripheral
   * registers region of RGB LEDs, knobs and line of yellow LEDs.
   */
  if (spiled_mem_base == NULL)
    exit(1);

  unsigned int last_uint_val_r;
  unsigned int uint_val_r;
  int temp;
  uint32_t color_limit;
  if(part_of_room == 0){
    color_limit = getColorLimit(units[UNITS_INDEX[user_select]].ceil_color);
  }
  else{
    color_limit = getColorLimit(units[UNITS_INDEX[user_select]].wall_color);
  }
  while (1) {
     uint32_t rgb_knobs_value;

     /* Initialize structure to 0 seconds and 200 milliseconds */
     //struct timespec loop_delay = {.tv_sec = 0, .tv_nsec = 200 * 1000 * 1000};
     rgb_knobs_value = *(volatile uint32_t*)(spiled_mem_base + SPILED_REG_KNOBS_8BIT_o);


     /* Assign value read from knobs to the basic signed and unsigned types */
     unsigned int press_button_new_uint_val = rgb_knobs_value>>24;
     uint_val_r = rgb_knobs_value;
     uint_val_r = (uint_val_r>>18)%64;

     if (isFirstTime == 1){
      last_uint_val_r = uint_val_r;
      isFirstTime = 0;
      continue;
     }

     temp = getIncrementation(uint_val_r, last_uint_val_r);
    if (temp != 2){
      changeIntensityTest(temp, part_of_room, user_select, color_limit);
    }
    if(part_of_room == ceiling){
        printRectangle(buffer, 284, 124, user_select, 0);
    }else if(part_of_room == wall){
        printRectangle(buffer, 284, 228, user_select, 1);

    }
    printBuffer(buffer);
     if (press_button_new_uint_val == 2){
     return;
     }
    
    last_uint_val_r = uint_val_r;
      
     }
  return;
}


void changeColor(unsigned *buffer, int user_select, const int part_of_room){
  const int ceiling = 0;
  const int wall = 1;
  unsigned int tempR = 0;
  unsigned int tempG = 0;
  unsigned int tempB = 0;
  //const int wall = 1;
  
      int isFirstTime = 1;
  /*
   * Setup memory mapping which provides access to the peripheral
   * registers region of RGB LEDs, knobs and line of yellow LEDs.
   */
  if (spiled_mem_base == NULL)
    exit(1);

  unsigned int last_uint_val_r;
  unsigned int uint_val_r;
  unsigned int last_uint_val_g;
  unsigned int uint_val_g;
  unsigned int last_uint_val_b;
  unsigned int uint_val_b;


  while (1) {
     uint32_t rgb_knobs_value;

     /* Initialize structure to 0 seconds and 200 milliseconds */
     //struct timespec loop_delay = {.tv_sec = 0, .tv_nsec = 200 * 1000 * 1000};
     rgb_knobs_value = *(volatile uint32_t*)(spiled_mem_base + SPILED_REG_KNOBS_8BIT_o);

     /* Assign value read from knobs to the basic signed and unsigned types */
     unsigned int press_button_new_uint_val = rgb_knobs_value>>24;
     uint_val_r = rgb_knobs_value;
     uint_val_r = (uint_val_r>>18)%64;
     uint_val_g = rgb_knobs_value;
     uint_val_g = (uint_val_g>>10)%64;
     uint_val_b = (rgb_knobs_value);
     uint_val_b = (uint_val_b>>2)%64;
     if (isFirstTime == 1){
       if(part_of_room == ceiling){
        tempR = (units[UNITS_INDEX[user_select]].ceil_color>>18)%64;
        tempG = (units[UNITS_INDEX[user_select]].ceil_color>>10)%64;
        tempB = (units[UNITS_INDEX[user_select]].ceil_color>>2)%64;
       }else if(part_of_room == wall){
        tempR = (units[UNITS_INDEX[user_select]].wall_color>>18)%64;
        tempG = (units[UNITS_INDEX[user_select]].wall_color>>10)%64;
        tempB = (units[UNITS_INDEX[user_select]].wall_color>>2)%64;
       }

       last_uint_val_r = uint_val_r;
       last_uint_val_g = uint_val_g;
       last_uint_val_b = uint_val_b;
       isFirstTime = 0;
     }
    
    tempR = getChangeOfColor(tempR, uint_val_r, last_uint_val_r);
    tempG = getChangeOfColor(tempG, uint_val_g, last_uint_val_g);
    tempB = getChangeOfColor(tempB, uint_val_b, last_uint_val_b);
    if(part_of_room == ceiling){
        units[UNITS_INDEX[user_select]].ceil_color = ((tempR)<<18) + (tempG<<10) + (tempB<<2);
    
        if(user_select == 0){
          changeColorOfLedCeil();
        }

        printRectangle(buffer, 284, 124, user_select, 0);
       }else if(part_of_room == wall){
        units[UNITS_INDEX[user_select]].wall_color = ((tempR)<<18) + (tempG<<10) + (tempB<<2);
          if(user_select == 0){
          changeColorOfLedWall();
        }
        printRectangle(buffer, 284, 228, user_select, 1);

       }
    printBuffer(buffer);

    if (press_button_new_uint_val == 2){
      return;
    }
    
    last_uint_val_r = uint_val_r;
    last_uint_val_g = uint_val_g;
    last_uint_val_b = uint_val_b;

    }  
}
void submenu(int user_select, unsigned *buffer){
  draw_submenu_background(buffer);
  print_submenu_text(user_select, buffer);
  submenu_button(buffer, 1);
  printBuffer(buffer);

  units[UNITS_INDEX[user_select]].inSubmenu = 1;

  int choice = 1;
  /*while loop*/
  int isFirstTime = 1;
  /*
   * Setup memory mapping which provides access to the peripheral
   * registers region of RGB LEDs, knobs and line of yellow LEDs.
   */
  if (spiled_mem_base == NULL)
    exit(1);
  unsigned int last_uint_val = 0;
  while (isPressed()){
  }
  printf("Checking\n");
  while (1) {
    changeColorOfLedWall();
    changeColorOfLedCeil();

     uint32_t rgb_knobs_value;
     int int_val;
     unsigned int uint_val;

     /* Initialize structure to 0 seconds and 200 milliseconds */
     //struct timespec loop_delay = {.tv_sec = 0, .tv_nsec = 200 * 1000 * 1000};
     rgb_knobs_value = *(volatile uint32_t*)(spiled_mem_base + SPILED_REG_KNOBS_8BIT_o);

    
     /* Assign value read from knobs to the basic signed and unsigned types */
     int_val = rgb_knobs_value;
     int_val = int_val>>18;
     uint_val = rgb_knobs_value;
     uint_val = uint_val>>18;
     //unsigned int press_button_new_uint_val = uint_val>>6;
     int press_button_new_int_val = int_val>>6;
     if (isFirstTime == 1){
       last_uint_val = uint_val;
       isFirstTime = 0;
     }

    if (uint_val > last_uint_val && int_val < 64){
      if(uint_val == 63 && last_uint_val == 0){
        if((choice  - 1) < 1){
          choice = 5;
        }else{
          --choice;
        }
      }else{
        if((choice + 1) > 5){
          choice = 1;
        }else{
          ++choice;
        }
      }
    
    submenu_button(buffer, choice);
     printBuffer(buffer);
     last_uint_val = uint_val;
    }else if(uint_val < last_uint_val && int_val < 64){
      if(uint_val == 0 && last_uint_val == 63){
        if((choice + 1) > 5){
        choice = 1;
      }else{
        ++choice;
      }
      }else{
        if((choice  - 1) < 1){
          choice = 5;
        }else{
          --choice;
        }
      }
    submenu_button(buffer, choice);
    printBuffer(buffer);
     last_uint_val = uint_val;
    }
    if (press_button_new_int_val == 4){
      printf("Call a function%d\n", choice);
      switch(choice){
        case 1:
          changeColor(buffer, user_select, 0);
          break;
        case 2:
          changeIntensity(buffer, user_select, 0);
          break;
        case 3:
          changeColor(buffer, user_select, 1);
          break;
        case 4:
          changeIntensity(buffer, user_select, 1);
          break;
        case 5:
          if(user_select != 0){
            units[UNITS_INDEX[user_select]].inSubmenu = 0;
            toBeSent = 1;
            indexOfSent = UNITS_INDEX[user_select];
          }
          return;
        default:
          break;
      }
    }
  }

}

void changeButton(unsigned *buffer, int choice, int number_of_rooms){

choice = choice -1;
int radius = 9;
int x0 = 30;
int y0 = 25 + choice * 30;
int i,j;
for (int k = 0; k < number_of_rooms; ++k){

  int y0_temp = 25 + k * 30;

  for (i = y0_temp-radius; i < y0_temp+radius ; i++) {
    for (j = x0-radius; j < x0+radius; j++) {
      if(((j-x0)*(j-x0) + (i-y0_temp)*(i-y0_temp)) < radius*radius){	
        buffer[i*480 + j] = 0xFFFFFF;
      }
    }
  }
}
  i = 0;
  j = 0;
  for (i = y0-radius; i < y0+radius ; i++) {
    for (j = x0-radius; j < x0+radius; j++) {
      if(((j-x0)*(j-x0) + (i-y0)*(i-y0)) < radius*radius){	
        buffer[i*480 + j] = 0xf800;
      }
    }
  }
}

int menu(){

int number_of_rooms = 0;
int choice = 1;
int i, j;

if (parlcd_mem_base == NULL)
  exit(1);

parlcd_hx8357_init(parlcd_mem_base);

parlcd_write_cmd(parlcd_mem_base, 0x2c);

unsigned buffer[320*480];  

//black
for (i = 0; i < 320; i++) {
  for (j = 0; j < 480 ; j++) {
	  buffer[i*480 + j] = 0;
  }
}
//white
for(int a = 0; a < 10; a++){
  for (i = 15; i < 35; i++) {
    for (j = 15; j < 465 ; j++) {
	    buffer[(i + (a*30))*480 + j] = 0xFFFFFF;
    }
  }
}

for(i = 0; i < UNITS_MAX; i++){
  if(units[i].exists){
    UNITS_INDEX[number_of_rooms] = i;
    printText(70, 17 + (number_of_rooms * 30), (unsigned char*)units[i].name, buffer);
    printIcon(280, 17 + (number_of_rooms * 30), units[i].icon_buffer, buffer);
    number_of_rooms++;
  }
}

changeButton(buffer, 1, number_of_rooms);

printBuffer(buffer);
int isFirstTime = 1;
  /*
   * Setup memory mapping which provides access to the peripheral
   * registers region of RGB LEDs, knobs and line of yellow LEDs.
   */
  if (spiled_mem_base == NULL)
    exit(1);
  unsigned int last_uint_val = 0;
  while (1) {

    changeColorOfLedWall();
    changeColorOfLedCeil();
    number_of_rooms = 0;
    for(i = 0; i < UNITS_MAX; i++){
      if(units[i].exists){
        UNITS_INDEX[number_of_rooms] = i;
        printText(70, 17 + (number_of_rooms * 30), (unsigned char*)units[i].name, buffer);
        printIcon(280, 17 + (number_of_rooms * 30), units[i].icon_buffer, buffer);
        number_of_rooms++;
      }
    }

    printBuffer(buffer);
     uint32_t rgb_knobs_value;
     int int_val;
     unsigned int uint_val;

     /* Initialize structure to 0 seconds and 200 milliseconds */
     //struct timespec loop_delay = {.tv_sec = 0, .tv_nsec = 200 * 1000 * 1000};
     rgb_knobs_value = *(volatile uint32_t*)(spiled_mem_base + SPILED_REG_KNOBS_8BIT_o);

     /* Assign value read from knobs to the basic signed and unsigned types */
     int_val = rgb_knobs_value;
     int_val = int_val>>18;
     uint_val = rgb_knobs_value;
     uint_val = uint_val>>18;
     //unsigned int press_button_new_uint_val = uint_val>>6;
     int press_button_new_int_val = int_val>>6;
     if (isFirstTime == 1){
       last_uint_val = uint_val;
       isFirstTime = 0;
     }

    if (uint_val > last_uint_val && int_val < 64){
      if(uint_val == 63 && last_uint_val == 0){
        if((choice  - 1) < 1){
          choice = number_of_rooms;
        }else{
          --choice;
        }
      }else{
        if((choice + 1) > number_of_rooms){
          choice = 1;
        }else{
          ++choice;
        }
      }
    changeButton(buffer, choice, number_of_rooms);
     printBuffer(buffer);
     last_uint_val = uint_val;
    }else if(uint_val < last_uint_val && int_val < 64){
      if(uint_val == 0 && last_uint_val == 63){
        if((choice + 1) > number_of_rooms){
        choice = 1;
      }else{
        ++choice;
      }
      }else{
        if((choice  - 1) < 1){
          choice = number_of_rooms;
        }else{
          --choice;
        }
      }
    changeButton(buffer, choice, number_of_rooms);
    printBuffer(buffer);
     last_uint_val = uint_val;
    }
    if (press_button_new_int_val == 4){
      break;
    }
  }
  return choice - 1;
}
uint32_t getOtherIp(const char* state_packet) {
	uint32_t ret = *((uint32_t*)(state_packet));
  ret = BIG_END ? ret : SWAP(ret);
	return ret;
}
uint32_t messageType(char *buffer){
  uint32_t type = *((uint32_t*)(buffer));
  printf("\n\n%u\n\n", type);
	return type;
}
void fillBoardCastMessageToSent(unsigned char* message, int index){
  *(int*)(message) = BIG_END ? units[index].IP : SWAP (units[index].IP);
  *(int*)(message+4) = BIG_END ? 0x00010000 : SWAP(0x00010000);
  *(int*)(message+8) = 1;
  *(int*)(message+12) = BIG_END ? units[index].ceil_color: SWAP( units[index].ceil_color);
  *(int*)(message+16) = BIG_END ? units[index].wall_color: SWAP( units[index].wall_color);
  strcpy(message+20,  units[index].name);
	memcpy(message+36,  units[index].icon_buffer, 512);
}
void fillBroadcastMessage(unsigned char* message){
 // uint32_t local_ip_address = get_local_ip_address();
  *(int*)(message) = BIG_END ? units[0].IP : SWAP (units[0].IP);
 // *(int*)(message) = local_ip_address;
  *(int*)(message+4) = BIG_END ? 0x00010000 : SWAP(0x00010000);
  *(int*)(message+8) = 0;
  *(int*)(message+12) = BIG_END ? units[UNITS_INDEX[MY_UNIT_INDEX]].ceil_color: SWAP( units[UNITS_INDEX[MY_UNIT_INDEX]].ceil_color);
  *(int*)(message+16) = BIG_END ? units[UNITS_INDEX[MY_UNIT_INDEX]].wall_color: SWAP( units[UNITS_INDEX[MY_UNIT_INDEX]].wall_color);
	
  strcpy(message+20,  units[UNITS_INDEX[MY_UNIT_INDEX]].name);
	memcpy(message+36,  units[UNITS_INDEX[MY_UNIT_INDEX]].icon_buffer, 512);
}

void fillName(char *name, char *buffer){
  for(int i = 0; i < 16; i++){
    name[i] = buffer[i];
  }
  name[16] = '\0';
}

void* recThreadFunction(void *arg){
  int sockfd;
	int reuse_port_enable = 1;
	struct sockaddr_in local;
//uint32_t local_ip_address = get_local_ip_address();
	//struct sockaddr_in from;

	memset(&local, 0, sizeof(local));
	local.sin_family = AF_INET;
	local.sin_port = htons(5555);
	local.sin_addr.s_addr = htonl(INADDR_ANY);

	// Create the socket
	if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) == -1) {
		perror("CREATE_RECEIVE_SOCKET_ERROR");
		exit(1);
	}

	if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &reuse_port_enable, sizeof(reuse_port_enable)) == -1) {
        perror("Setsockopt\n");
        exit(2);
    }


	// Bind the socket to local sockaddr_in
	if (bind(sockfd, (struct sockaddr *)&local, sizeof(local)) == -1) {
		perror("BIND_RECEIVER_SOCKET_ERROR");
		exit(1);
	}


	char *packet_buffer = calloc(1, 1024);

	// Start listening to incoming packets
	while (1) {
		if (recv(sockfd, packet_buffer, 1024, 0) == -1) {
			perror("RECEIVE_ERROR");
			exit(1);
		}else{
      if(messageType(packet_buffer + 8)==1){
        printf("type1)\n");
        if (getOtherIp(packet_buffer) == units[0].IP) {
          uint32_t ceil1 = *((uint32_t*)(packet_buffer + 12)); 
          uint32_t wall1 = *((uint32_t*)(packet_buffer + 16)); 
          units[0].ceil_color = BIG_END? ceil1:SWAP(ceil1);
          units[0].wall_color = BIG_END? wall1:SWAP(wall1);
          fillName(units[0].name, (packet_buffer + 20));
          units[0].exists = 1;
          memcpy(units[0].icon_buffer, packet_buffer+36, 512);
        }
      }
      else{
        if (getOtherIp(packet_buffer) != units[0].IP) {
          printf("HOLA");
          char inUnits = 0;
          for(int i = 0; i < UNITS_MAX; i++){
            if(units[i].exists && getOtherIp(packet_buffer) == units[i].IP && units[i].inSubmenu != 1){
              uint32_t ceil1 = *((uint32_t*)(packet_buffer + 12)); 
              uint32_t wall1 = *((uint32_t*)(packet_buffer + 16)); 
              units[i].ceil_color = BIG_END? ceil1:SWAP(ceil1);
              units[i].wall_color = BIG_END? wall1:SWAP(wall1);
              fillName(units[i].name, (packet_buffer + 20));
              units[i].exists = 1;
              memcpy(units[i].icon_buffer, packet_buffer+36, 512);
              inUnits = 1;
              break;
            }
          }
          if(!inUnits){
            for(int i = 0; i < UNITS_MAX; i++){
              if(units[i].IP == getOtherIp(packet_buffer)){
                break;
              }
              if(!units[i].exists){
                units[i].IP = getOtherIp(packet_buffer);
                uint32_t ceil1 = *((uint32_t*)(packet_buffer + 12)); 
                uint32_t wall1 = *((uint32_t*)(packet_buffer + 16)); 
                units[i].ceil_color = BIG_END? ceil1:SWAP(ceil1);
                units[i].wall_color = BIG_END? wall1:SWAP(wall1);
                fillName(units[i].name, (packet_buffer + 20));
                units[i].exists = 1;
                memcpy(units[i].icon_buffer, packet_buffer+36, 512);
                break;
              }
            }
          }
        }   
      } 
    }
  }
  return NULL;
}

void *statusThreadFunction(void *arg){
  unsigned char message[1024] = {0};
  unsigned char message1[1024] = {0};
  int sockfd;
  if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) == -1) {
      perror("socket");
      exit(1);
  }
  struct sockaddr_in broadcast_addr;
  int yes=1;

  if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes)) == -1) {
    perror("setsockopt (SO_REUSEADDR)");
    exit(1);
  }
  int broadcast = 1;

  if (setsockopt(sockfd, SOL_SOCKET, SO_BROADCAST, &broadcast, sizeof broadcast) == -1) {
      perror("setsockopt (SO_BROADCAST)");
      exit(1);
  }

  memset(&broadcast_addr, 0, sizeof(broadcast_addr));
  broadcast_addr.sin_family = AF_INET;
  broadcast_addr.sin_port = htons(5555);
  broadcast_addr.sin_addr.s_addr = INADDR_BROADCAST;

  if (bind(sockfd, (struct sockaddr *)&broadcast_addr, sizeof(broadcast_addr)) == -1) {
    perror("bind");
    exit(1);
  }
  while(1){
    if(toBeSent == 1){
      fillBoardCastMessageToSent(message1, indexOfSent);
      if (sendto(sockfd, message1, 1024, 0, (struct sockaddr *)&broadcast_addr, sizeof(broadcast_addr)) == -1 ){
        perror("broadcast failed");
        exit(1);
      }
      toBeSent = 0;
    }else{
      fillBroadcastMessage(message);
      if (sendto(sockfd, message, 1024, 0, (struct sockaddr *)&broadcast_addr, sizeof(broadcast_addr)) == -1 ){
        perror("broadcast failed");
        exit(1);
      }
    }
    delay(1000);
  }
  return NULL;
}

int main(int argc, char *argv[])
{  
  
  struct ifaddrs *ifaddress;
  uint32_t ip_address = 0;

  if (getifaddrs(&ifaddress) == -1) {
      perror("getifaddrs");
      return 1;
  }

  while(ifaddress != NULL){
    if(ifaddress->ifa_addr != NULL &&
    ((struct sockaddr_in *)(ifaddress->ifa_addr))->sin_addr.s_addr > 64 && 
    ifaddress->ifa_flags == 69699){
      ip_address = ntohl(((struct sockaddr_in *)(ifaddress->ifa_addr))->sin_addr.s_addr);
      printf("ip_address = %u, %u, %u, %d\n", ip_address, ((struct sockaddr_in *)(ifaddress->ifa_addr))->sin_addr.s_addr, ifaddress->ifa_flags, ifaddress->ifa_addr->sa_family);
      break;
    }
    ifaddress = ifaddress->ifa_next;
  }
  if(ip_address == 0){
    perror("local IP not pound");
    exit(1);
  }
  units[0].IP = ip_address;


  printf("q\n");

  parlcd_mem_base = map_phys_address(PARLCD_REG_BASE_PHYS, PARLCD_REG_SIZE, 0);
  spiled_mem_base = map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);
  if (parlcd_mem_base == NULL)
    exit(1);

  parlcd_hx8357_init(parlcd_mem_base);

  parlcd_write_cmd(parlcd_mem_base, 0x2c);

  unsigned buffer[320*480];
  init_unit_struct();

  //threads
  pthread_t recThread, statusThread;

  pthread_create(&recThread, NULL, recThreadFunction, NULL);
  pthread_create(&statusThread, NULL, statusThreadFunction, NULL);
  

  while(1){
    int room = menu();
    printf("%d\n", room);
    submenu(room, buffer);
  }
  return 0;
}
